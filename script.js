function dark(){
    const body = document.body;
    const toggleButton = document.getElementById('toggle-mode');
    
    function toggleMode() {
        const newMode = !body.classList.toggle('dark-mode');
        localStorage.setItem('darkMode', newMode);
    }
    
    const isDarkMode = localStorage.getItem('darkMode') === 'true';
    
    if (isDarkMode) {
        body.classList.add('dark-mode');
    }
    
    toggleButton.addEventListener('click', toggleMode);
}

