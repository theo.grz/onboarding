<?php
session_start();
// Incluez ici la configuration de votre base de données
$serveur = "localhost";  // Remplacez par l'adresse de votre serveur MySQL
$utilisateur = "root";  // Remplacez par le nom d'utilisateur de votre base de données
$motDePasse = "";  // Remplacez par le mot de passe de votre base de données
$nomBaseDeDonnees = "sportcompagny";  // Remplacez par le nom de votre base de données

// Établir une connexion à la base de données
$connexion = new mysqli($serveur, $utilisateur, $motDePasse, $nomBaseDeDonnees);

// Vérifier la connexion
if ($connexion->connect_error) {
    die("La connexion à la base de données a échoué : " . $connexion->connect_error);
}

// Traitement de l'inscription lorsque le formulaire est soumis
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["inscription"])) {
    $nom = $_POST["nom"];
    $prenom = $_POST["prenom"];
    $email = $_POST["email"];
    $mot_de_passe = password_hash($_POST["mot_de_passe"], PASSWORD_DEFAULT); // Hacher le mot de passe

    // Préparer et exécuter la requête SQL pour insérer les données dans la table "utilisateurs"
    $requete = "INSERT INTO user (nom, prenom, email, psw) VALUES (?, ?, ?, ?)";
    $statement = mysqli_prepare($connexion, $requete);
    mysqli_stmt_bind_param($statement, "ssss", $nom, $prenom, $email, $mot_de_passe);

    if (mysqli_stmt_execute($statement)) {
        // L'inscription a réussi
        echo "Inscription réussie !";
    } else {
        // L'inscription a échoué
        echo "Erreur lors de l'inscription : " . mysqli_error($connexion);
    }
}

// Traitement de la connexion lorsque le formulaire est soumis
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["connexion"])) {
    $email_connexion = $_POST["email_connexion"];
    $mot_de_passe_connexion = $_POST["mot_de_passe_connexion"];

    // Requête SQL pour récupérer l'utilisateur par email
    $requete = "SELECT id, email, psw, role FROM user WHERE email = ?";
    $statement = mysqli_prepare($connexion, $requete);
    mysqli_stmt_bind_param($statement, "s", $email_connexion);
    mysqli_stmt_execute($statement);
    mysqli_stmt_store_result($statement);
    

    if (mysqli_stmt_num_rows($statement) == 1) {
        // Utilisateur trouvé, vérifier le mot de passe
        mysqli_stmt_bind_result($statement, $utilisateur_id, $utilisateur_email, $utilisateur_mot_de_passe, $utilisateur_role);
        mysqli_stmt_fetch($statement);

        // Ou utilisation de echo pour afficher les résultats
        echo "ID : " . $utilisateur_id . "<br>";
        echo "Email : " . $utilisateur_email . "<br>";
        echo "Mot de passe : " . $utilisateur_mot_de_passe . "<br>";
        echo "Rôle : " . $utilisateur_role . "<br>";

        if (password_verify($mot_de_passe_connexion, $utilisateur_mot_de_passe)) {
            // Mot de passe correct, connectez l'utilisateur
            $_SESSION["role"] = $utilisateur_role; // Stockez le rôle en session
            echo "Connexion réussie pour l'utilisateur avec l'ID : " . $utilisateur_id;
        } else {
            // Mot de passe incorrect
            echo "Mot de passe incorrect";
        }
        
    } else {
        // Aucun utilisateur trouvé avec cet email
        echo "Aucun utilisateur trouvé avec cet email";
    }
}

// Fonction pour obtenir tous les produits
function getAllProducts($connexion) {
    $requeteProduits = "SELECT * FROM produit";
    $resultatProduits = $connexion->query($requeteProduits);
    return $resultatProduits;
}

// Fonction pour supprimer un utilisateur
function supprimerUtilisateur($connexion, $id_utilisateur_a_supprimer) {
    // Préparez et exécutez la requête SQL pour supprimer l'utilisateur
    $requete_suppression = "DELETE FROM user WHERE id = ?";
    $statement_suppression = mysqli_prepare($connexion, $requete_suppression);
    mysqli_stmt_bind_param($statement_suppression, "i", $id_utilisateur_a_supprimer);

    if (mysqli_stmt_execute($statement_suppression)) {
        // La suppression a réussi
        return true;
    } else {
        // La suppression a échoué
        return false;
    }
}

// Vérifiez si une action spécifique est demandée dans l'URL
if (isset($_GET['action'])) {
    if ($_GET['action'] === 'supprimer' && isset($_GET['id'])) {
        $id_utilisateur_a_supprimer = $_GET['id'];

        // Appel de la fonction pour supprimer l'utilisateur
        if (supprimerUtilisateur($connexion, $id_utilisateur_a_supprimer)) {
            header("Location: admin.php");
            exit();
        } else {
            // Une erreur s'est produite lors de la suppression
            echo "Erreur lors de la suppression de l'utilisateur : " . mysqli_error($connexion);
        }
    }
}

//$connexion->close();
?>

