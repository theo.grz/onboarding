<!DOCTYPE html>
<html lang="en">
<?php 
require "route.php";

if (isset($_SESSION["role"]) && $_SESSION["role"] == 1) {
    echo "tu es admin";
} else {
    echo "Vous n'avez pas les autorisations pour accéder à cette page.";
    exit(); 
}

?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin</title>
    <link rel="stylesheet" href="../style.css">
    <script src="../script.js"></script>
</head>
<style>
    footer{
        margin-top: 12%;
    }
</style>
<body>
    <nav>
        <img src="../images/img.png" alt="Logo Sport Company">
        <ul>
            <li><a href="../templates/index.php">Accueil</a></li>
            <li><a href="../templates/produits.php">Produits</a></li>
            <li><a href="../templates/contact.php">Contact</a></li>
        </ul>
    </nav>
    <input type="button" id="toggle-mode" value="🌙" onclick="dark()">
    <?php
$requete = "SELECT * FROM user";
$resultat = $connexion->query($requete);

if ($resultat->num_rows > 0) {
    $utilisateurs = $resultat->fetch_all(MYSQLI_ASSOC);
?>
    <h1>Liste des Utilisateurs</h1>
    <table>
        <thead>
            <tr>
                <th>Nom</th>
                <th>Email</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($utilisateurs as $utilisateur) {
                ?>
                <tr>
                    <td><?php echo $utilisateur["nom"]; ?></td>
                    <td><?php echo $utilisateur["email"]; ?></td>
                    <td>
                        <a href="modifier_utilisateur.php?id=<?php echo $utilisateur["id"]; ?>">Modifier</a>
                        <a href="route.php?action=supprimer&id=<?php echo $utilisateur["id"]; ?>" onclick="return confirm('Êtes-vous sûr de vouloir supprimer cet utilisateur ?')">Supprimer</a>

                    </td>   
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
<?php
} else {
    echo "Aucun utilisateur trouvé dans la base de données.";
}
?>


    <footer>
        <p>&copy; 2023 Sport Company</p>
    </footer>
</body>
</html>
