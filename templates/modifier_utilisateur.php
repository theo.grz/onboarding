<?php
require "route.php"; 
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
    <script src="../script.js"></script>
    <title> UpdateUser</title>
</head>
<body onload="affiche()">
  
    <nav>
        <img src="../images/img.png" alt="Logo Sport Company">
        <ul>
            <li><a href="../templates/index.php">Accueil</a></li>
            <li><a href="../templates/produits.php">Produits</a></li>
            <li><a href="../templates/contact.php">Contact</a></li>
            <li><a href="../templates/inscription.php">Inscription</a></li>
            <li><a href="../templates/admin.php">admin</a></li>

        </ul>
    </nav>

    
    <input type="button" id="toggle-mode" value="🌙" onclick="dark()">
    <script>
        const body = document.body;
        const toggleButton = document.getElementById('toggle-mode');

        function toggleMode() {
            const newMode = !body.classList.toggle('dark-mode');
            localStorage.setItem('darkMode', newMode);
        }

        const isDarkMode = localStorage.getItem('darkMode') === 'true';

        if (isDarkMode) {
            body.classList.add('dark-mode');
        }

        toggleButton.addEventListener('click', toggleMode);
    </script>
<?php
if (isset($_GET['id'])) {
    $id_utilisateur = $_GET['id'];

    if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["modifier"])) {
        $nouveau_nom = $_POST["nouveau_nom"];
        $nouveau_email = $_POST["nouveau_email"];

        $requete = "UPDATE user SET nom = ?, email = ? WHERE id = ?";
        $statement = mysqli_prepare($connexion, $requete);
        mysqli_stmt_bind_param($statement, "ssi", $nouveau_nom, $nouveau_email, $id_utilisateur);

        if (mysqli_stmt_execute($statement)) {
            echo "Utilisateur mis à jour avec succès !";
        } else {
            echo "Erreur lors de la mise à jour de l'utilisateur : " . mysqli_error($connexion);
        }
    }

    $requete = "SELECT * FROM user WHERE id = ?";
    $statement = mysqli_prepare($connexion, $requete);
    mysqli_stmt_bind_param($statement, "i", $id_utilisateur);
    mysqli_stmt_execute($statement);
    $resultat = mysqli_stmt_get_result($statement);

    if ($resultat->num_rows == 1) {
        $utilisateur = $resultat->fetch_assoc();
        ?>
        <h1>Modifier l'Utilisateur</h1>
        <form action="modifier_utilisateur.php?id=<?php echo $id_utilisateur; ?>" method="post">
            <div class="mb-3">
                <label for="nouveau_nom" class="form-label">Nouveau Nom</label>
                <input type="text" class="form-control" name="nouveau_nom" value="<?php echo $utilisateur["nom"]; ?>">
            </div>
            <div class="mb-3">
                <label for="nouveau_email" class="form-label">Nouveau Email</label>
                <input type="email" class="form-control" name="nouveau_email" value="<?php echo $utilisateur["email"]; ?>">
            </div>
            <button type="submit" class="btn btn-primary" name="modifier">Modifier</button>
        </form>
        <?php
    } else {
        echo "Aucun utilisateur trouvé avec cet ID.";
    }
} else {
    echo "ID de l'utilisateur non spécifié.";
}
?>
<footer>
        <p>&copy; 2023 Sport Company</p>
    </footer>
    </body>
</html>

