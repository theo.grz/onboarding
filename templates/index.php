<!DOCTYPE html>
<html lang="fr">
<?php 
require "route.php"
?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
    <script src="../script.js"></script>
    <title> Sport Company</title>
</head>


<body onload="affiche()">
  
    <nav>
        <img src="../images/img.png" alt="Logo Sport Company">
        <ul>
            <li><a href="../templates/index.php">Accueil</a></li>
            <li><a href="../templates/produits.php">Produits</a></li>
            <li><a href="../templates/contact.php">Contact</a></li>
            <li><a href="../templates/admin.php">Admin</a></li>
            <li><a href="../templates/connexion.php">Connexion</a></li>
            <li><form action="deconnexion.php" method="post">
                <a href="">Déconnexion</a>
                </form>
            </li>
        </ul>
    </nav>

    <input type="button" id="toggle-mode" value="🌙" onclick="dark()">
    
    <header>
        <h1>Bienvenue Sport Company</h1>
    </header>

    <section>
        <h2>Notre Histoire</h2>
        <p>Depuis 1952, Sport Company est dédiée à l'amour du sport et à l'engagement envers la qualité. Fondée par John Smith, un passionné de sport, notre entreprise est née de son désir de fournir aux athlètes et aux passionnés de sport les meilleurs équipements et vêtements possibles.</p>
    
        <h3>La Naissance d'une Passion</h3>
        <p>Tout a commencé dans un petit magasin de sport local à New York. John, un athlète accompli, a ressenti le besoin d'améliorer la qualité des produits disponibles sur le marché. Il a décidé de créer sa propre entreprise pour proposer des produits de haute performance et des vêtements de sport élégants.</p>
    
        <h3>L'Évolution au Fil du Temps</h3>
        <p>Au cours des décennies, Sport Company a évolué et s'est adaptée aux besoins changeants des sportifs du monde entier. De l'introduction de notre première chaussure de course révolutionnaire dans les années 60 à la création de collections de vêtements de sport de renommée mondiale dans les années 80, nous avons constamment repoussé les limites de l'innovation.</p>
    
        <h3>Notre Engagement envers la Qualité</h3>
        <p>Chez Sport Company, nous croyons fermement que la qualité est la clé du succès. Tous nos produits sont fabriqués avec les meilleurs matériaux disponibles et soumis à des tests rigoureux pour garantir leur durabilité et leur performance. Nous ne faisons aucun compromis sur la qualité.</p>
    
        <h2>Notre Gamme de Produits</h2>
        <h3>Équipements de Sport</h3>
        <ul>
            <li>Chaussures de sport pour toutes les disciplines.</li>
            <li>Vêtements de sport haut de gamme.</li>
            <li>Équipements et accessoires pour le fitness et l'entraînement.</li>
        </ul>
    
        <h3>Collection Vintage</h3>
        <p>Revivez l'histoire du sport avec notre collection de vêtements et d'accessoires rétro inspirés des décennies passées.</p>
    
        <h3>Technologie de Pointe</h3>
        <p>Découvrez nos produits innovants, conçus avec les dernières avancées technologiques pour améliorer vos performances sportives.</p>
    
        <h2>Notre Engagement Social</h2>
        <p>Chez Sport Company, nous sommes fiers de redonner à la communauté sportive. Nous soutenons activement des programmes locaux et internationaux visant à promouvoir l'activité physique, à soutenir les athlètes en herbe et à encourager la diversité dans le sport.</p>
    
        <h2>Rejoignez la Famille Sport Company</h2>
        <p>Depuis plus de 70 ans, Sport Company a été le choix privilégié des athlètes et des amateurs de sport du monde entier. Notre dévouement à l'excellence et à la qualité continue de nous guider vers l'avenir.</p>
        <p>Joignez-vous à nous dans notre voyage pour célébrer le sport, l'innovation et la passion. Explorez notre gamme de produits et découvrez ce qui fait de Sport Company votre partenaire de confiance dans le monde du sport.</p>
        <p>Pour en savoir plus sur nos produits et nos initiatives, visitez notre site Web à <a href="www.sportcompany.com">www.sportcompany.com</a>.</p>
    
    </section>
    

    <footer>
        <p>&copy; 2023 Sport Company</p>
    </footer>
</body>
<script>
function affiche() {
  alert("Validation des paramètres de confidentialité");
}
</script>
</html>