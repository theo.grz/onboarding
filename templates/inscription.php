<!DOCTYPE html>
<html lang="en">
<?php 
require "route.php"
?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
    <script src="../script.js"></script>
    <title>Inscription</title>
</head>
<style>
  footer{
    margin-top: 3%;
  }
</style>
<body>
  <nav>
    <img src="../images/img.png" alt="Logo Sport Company">
    <ul>
            <li><a href="../templates/index.php">Accueil</a></li>
            <li><a href="../templates/produits.php">Produits</a></li>
            <li><a href="../templates/contact.php">Contact</a></li>
            <li><a href="../templates/admin.php">Admin</a></li>
            <li><a href="../templates/connexion.php">Connexion</a></li>
    </ul>
</nav>

<input type="button" id="toggle-mode" value="🌙" onclick="dark()">

  <h1>Formulaire d'inscription</h1>
    <form action="index.php" method="post">
    <input type="hidden" name="inscription" value="1">
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Nom</label>
            <input type="text" class="form-control" name="nom">
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Prenom</label>
            <input type="text" class="form-control" name="prenom">
        </div>
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label">Adresse mail</label>
          <input type="email" class="form-control" name="email" aria-describedby="emailHelp">
        </div>
        <div class="mb-3">
          <label for="exampleInputPassword1" class="form-label">Mot de passe</label>
          <input type="password" class="form-control" name="mot_de_passe">
        </div>
        <div class="mb-3 form-check">
          <input type="checkbox" class="form-check-input" id="exampleCheck1">
          <label class="form-check-label" for="exampleCheck1">Se souvenir de moi</label>
        </div>
        <button type="submit" class="btn btn-primary">S'enregistrer</button>
      </form>
      <footer>
        <p>&copy; 2023 Sport Company</p>
    </footer>
</body>
</html>