<!DOCTYPE html>
<html lang="en">
<?php 
require "route.php"
?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Liste de Produits</title>
    <link rel="stylesheet" href="../style.css">
    <script src="../script.js"></script>
</head>
<style>
        footer{
            margin-top: 2%;
        }
</style>
<body>
    
    <nav>
        <img src="../images/img.png" alt="Logo Sport Company">
        <ul>
            <li><a href="../templates/index.php">Accueil</a></li>
            <li><a href="../templates/produits.php">Produits</a></li>
            <li><a href="../templates/contact.php">Contact</a></li>
            <li><a href="../templates/connexion.php">Connexion</a></li>
        </ul>
    </nav>

       <input type="button" id="toggle-mode" value="🌙" onclick="dark()">
    <script>
        const body = document.body;
        const toggleButton = document.getElementById('toggle-mode');

        function toggleMode() {
            const newMode = !body.classList.toggle('dark-mode');
            localStorage.setItem('darkMode', newMode);
        }

        const isDarkMode = localStorage.getItem('darkMode') === 'true';

        if (isDarkMode) {
            body.classList.add('dark-mode');
        }

        toggleButton.addEventListener('click', toggleMode);
    </script>
    <input type="text" id="search" placeholder="Rechercher un produit...">
    <div id="sort-buttons">
        <button id="sort-price">Trier par Prix</button>
        <button id="sort-name">Trier par Nom</button>
    </div>

    <ul id="product-list">
    <?php
    $resultatProduits = getAllProducts($connexion);
    // var_dump($resultatProduits);
    foreach ($resultatProduits as $produit) {
        ?>
        <li class="product">
            <h2><?php echo $produit['nameProduit']; ?></h2>
            <p>ID: <?php echo $produit['id_produit']; ?></p>
            <p>Prix: $<?php echo $produit['prix']; ?></p>
        </li>
        <?php
    }
    ?>
</ul>

    <script>
        const searchInput = document.getElementById('search');
        const productList = document.getElementsByClassName('product');
        const sortPriceButton = document.getElementById('sort-price');
        const sortNameButton = document.getElementById('sort-name');
        const originalProductList = Array.from(productList);

        // Fonction pour trier par prix
        sortPriceButton.addEventListener('click', function () {
            const sortedList = originalProductList.slice().sort((a, b) => {
                const priceA = parseFloat(a.querySelector('p:last-child').textContent.split(':')[1].trim().replace('$', ''));
                const priceB = parseFloat(b.querySelector('p:last-child').textContent.split(':')[1].trim().replace('$', ''));
                return priceA - priceB;
            });

            // Réinsère les éléments triés dans la liste
            const productListElement = document.getElementById('product-list');
            while (productListElement.firstChild) {
                productListElement.removeChild(productListElement.firstChild);
            }
            for (let i = 0; i < sortedList.length; i++) {
                productListElement.appendChild(sortedList[i]);
            }
        });

        // Fonction pour trier par nom
        sortNameButton.addEventListener('click', function () {
            const sortedList = originalProductList.slice().sort((a, b) => {
                const nameA = a.querySelector('h2').textContent.toLowerCase();
                const nameB = b.querySelector('h2').textContent.toLowerCase();
                return nameA.localeCompare(nameB);
            });

            // Réinsère les éléments triés dans la liste
            const productListElement = document.getElementById('product-list');
            while (productListElement.firstChild) {
                productListElement.removeChild(productListElement.firstChild);
            }
            for (let i = 0; i < sortedList.length; i++) {
                productListElement.appendChild(sortedList[i]);
            }
        });

        searchInput.addEventListener('input', function () {
            const searchTerm = searchInput.value.toLowerCase();
            
            for (let i = 0; i < productList.length; i++) {
                const product = productList[i];
                const productText = product.textContent.toLowerCase();
                
                if (productText.includes(searchTerm)) {
                    product.style.display = 'block';
                } else {
                    product.style.display = 'none';
                }
            }
        });
    </script>
    <footer>
        <p>&copy; 2023 Sport Company</p>
    </footer>
</body>
</html>
