<!DOCTYPE html>
<html lang="en">
<?php 
require "route.php"
?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contact</title>
    <link rel="stylesheet" href="../style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
        crossorigin="anonymous"></script>
    <script src="../script.js"></script>
</head>
<body>
    <nav>
        <img src="../images/img.png" alt="Logo Sport Company">
        <ul>
            <li><a href="../templates/index.php">Accueil</a></li>
            <li><a href="../templates/produits.php">Produits</a></li>
            <li><a href="../templates/contact.php">Contact</a></li>
            <li><a href="../templates/connexion.php">Connexion</a></li>
        </ul>
    </nav>
    <input type="button" id="toggle-mode" value="🌙" onclick="dark()">
    <div class="containerContact">
        <h1>Contacter nous</h1>

        <p>Adresse: 8 rue du Rivoli</p>
        <p>75004 PARIS</p>
        <p>Tel: 06.15.45.98.78</p>
        <div class="containerIFrame">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2624.395804728476!2d2.294481315123705!3d48.858844079288816!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e66e1f06e2b70f%3A0x40b82c3688c9460!2sEiffel%20Tower!5e0!3m2!1sen!2sus!4v1609871961859!5m2!1sen!2sus" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
    </div>
    
    <footer>
        <p>&copy; 2023 Sport Company</p>
    </footer>
</body>
</html>