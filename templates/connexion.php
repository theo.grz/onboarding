<!DOCTYPE html>
<html lang="en">
<?php 
require "route.php"
?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Connexion</title>
    <link rel="stylesheet" href="../style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
        crossorigin="anonymous"></script>
    <script src="../script.js"></script>
</head>

<body>
    <nav>
        <img src="../images/img.png" alt="Logo Sport Company">
        <ul>
            <li><a href="../templates/index.php">Accueil</a></li>
            <li><a href="../templates/produits.php">Produits</a></li>
            <li><a href="../templates/contact.php">Contact</a></li>
            <li><a href="../templates/inscription.php">Inscription</a></li>
        </ul>
    </nav>
    <style>
        footer{
            margin-top: 13%;
        }
    </style>
    <input type="button" id="toggle-mode" value="🌙" onclick="dark()">
    <h1>Se connecter</h1>
    
    <form action="index.php" method="POST">
        <div class="containerConnexion">
            <div class="row g-3 align-items-center">
                <input type="hidden" name="connexion" value="1">
                <div class="col-auto">
                    <label for="email" class="col-form-label">Email</label>
                </div>
                <div class="col-auto">
                    <input type="email" id="email" name="email_connexion" class="form-control">
                </div>
                <div class="row g-3 align-items-center">
                    <div class="col-auto">
                        <label for="inputPassword6" class="col-form-label" name="mot_de_passe">Mot de passe</label>
                    </div>
                    <div class="col-auto">
                        <input type="password" id="inputPassword6" name="mot_de_passe_connexion" class="form-control"
                            aria-describedby="passwordHelpInline">
                    </div>
                </div>
                <div class="col-auto">
                    <button type="submit" class="btn btn-primary mb-3">Confirmer</button>
                </div>
            </div>
        </div>
    </form>
    

        <footer>
            <p>&copy; 2023 Sport Company</p>
        </footer>
</body>

</html>